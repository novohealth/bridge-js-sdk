const iframes = {}
const events = {}
const errMsg = 'failed to load'

const emitToApp = (event, data, name) => {
  if (!name) {
    for (var i = 0; i < frames.length; i++) {
      emitToApp(event, data, frames[i].name)
    }
    return
  }

  const frame = window.frames[name]
  if (frame) {
    frame.postMessage(
      JSON.stringify({
        event,
        data,
        name,
      }),
      window.origin
    )
  }
}

const emitToParent = (event, data) => {
  window.parent.window.postMessage(
    JSON.stringify({
      event,
      data,
      name: window.name,
    }),
    window.origin
  )
}

const on = (evt, handler) => {
  events[evt] = handler
}

const loadApp = (name, url, targetEl) => {
  return new Promise((resolve, reject) => {
    const frame = document.createElement('iframe')
    frame.name = name
    frame.src = url
    frame.setAttribute('scrolling', 'no')
    frame.setAttribute('marginheight', '0')
    frame.setAttribute('frameborder', '0')
    frame.setAttribute('allowtransparency', 'true')
    frame.setAttribute(
      'allow',
      'camera *;microphone *;clipboard-read *;clipboard-write *'
    )
    frame.width = '100%'
    frame.height = '100%'
    frame.addEventListener(
      'load',
      () => {
        let frameContentDocument
        try {
          frameContentDocument = frame.contentDocument
        } catch (e) {
          return reject({ name, message: errMsg })
        }
        if (frameContentDocument) {
          return resolve()
        }
        return reject({ name, message: errMsg })
      },
      false
    )
    targetEl.append(frame)
  })
}

window.addEventListener(
  'message',
  (evt) => {
    if (evt.origin !== location.origin) {
      let payload
      try {
        payload = JSON.parse(evt.data)
        const handler = events[payload.event]
        if (handler) {
          handler(payload)
        }
      } catch (e) {
        console.warn('mingle err:', e.message)
      }
    }
  },
  false
)
