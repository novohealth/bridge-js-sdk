'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

/**
 * @internal
 */
const LOG_DEBUG = 2;
const LOG_NONE = 0;
const REQUESTS = {
    GET_PATIENT: 'get_patient',
    IS_DISPLAYING: 'is_displaying',
    SHOW_APP: 'show_app',
    UPDATE_AUTH: 'update_auth',
    UPDATE_PATIENT: 'patient_changed',
    UPDATE_BADGE_COUNT: 'badge_count_changed',
};
const EVENTS = {
    AUTH_CHANGED: 'auth_changed',
    DISPLAY_CHANGED: 'display_changed',
    PATIENT_CHANGED: 'patient_changed',
};

const origins = {};
const PARENT_ORIGIN_CHANGED = 'parentOriginChanged';
const PARENT = 'parent';
const PREFIX = '$$';
const emitToApp = (event, data, name) => {
    const frame = window.frames[name];
    if (frame) {
        frame.postMessage(JSON.stringify({
            data,
            event,
        }), origins[name]);
    }
};
const emitToParent = (event, data = null) => {
    window.parent.window.postMessage(JSON.stringify({
        app: window.name,
        data,
        event,
    }), origins[PARENT]);
};
const on = (event, handle) => {
    const f = (payload) => {
        if (payload.app) {
            // dispatch to parent
            handle({
                app: payload.app,
                data: payload.data,
            });
        }
        else {
            // dispatch to child
            handle(payload.data);
        }
    };
    window.addEventListener(PREFIX + event, (evt) => {
        f(evt.detail);
    });
    return () => {
        return window.removeEventListener(PREFIX + event, f);
    };
};
const loadApp = (name, url, targetEl) => {
    origins[name] = new URL(url).origin;
    return new Promise((resolve, reject) => {
        const frame = document.createElement('iframe');
        frame.name = name;
        frame.src = url;
        frame.setAttribute('scrolling', 'no');
        frame.setAttribute('marginheight', '0');
        frame.setAttribute('frameborder', '0');
        frame.setAttribute('allowtransparency', 'true');
        frame.setAttribute('allow', 'camera *;microphone *;clipboard-read *;clipboard-write *');
        frame.width = '100%';
        frame.height = '100%';
        frame.addEventListener('load', () => {
            emitToApp(PARENT_ORIGIN_CHANGED, location.origin, name);
            return resolve();
        }, false);
        targetEl.append(frame);
    });
};
on(PARENT_ORIGIN_CHANGED, (origin) => {
    origins[PARENT] = origin;
});
window.addEventListener('message', (evt) => {
    if (evt.origin !== location.origin) {
        let payload;
        try {
            payload = JSON.parse(evt.data);
            console.log('DISPATCHING CUSTOM EVENT:', payload);
            window.dispatchEvent(new CustomEvent(PREFIX + payload.event, { detail: payload }));
        }
        catch (e) {
            console.warn('[ MINGLE ERROR ] ::', e.message);
        }
    }
}, false);

let _logLevel = LOG_NONE;
/**
 * Logs items to console window in the browser
 *
 * @param logLevel log level for logging
 * @param rest all the argument followed will be printed to the console
 */
function log(logLevel, ...rest) {
    if (_logLevel >= logLevel) {
        console.log(...rest);
    }
}

/**
 * Subscribe to the auth user change event. Applicable to Novo applications only.
 *
 * @param handle
 *
 * @private
 */
const onAuthChanged = (handle) => {
    log(LOG_DEBUG, 'bridge :: onAuthChanged:', handle);
    return on(REQUESTS.UPDATE_AUTH, handle);
};
/**
 * Subscribe when application badge count changes
 *
 * @param handle
 *
 * @private
 */
const onBadgeCountChanged = (handle) => {
    return on(REQUESTS.UPDATE_BADGE_COUNT, handle);
};
/**
 * Subscribe when application requests current patient
 *
 * @param handle
 * @returns
 *
 * @private
 */
const onGetPatient = (handle) => {
    return on(REQUESTS.GET_PATIENT, handle);
};
/**
 * Subscribe when application requests current display status
 *
 * @param handle
 * @returns
 *
 * @private
 */
const onGetDisplayStatus = (handle) => {
    return on(REQUESTS.IS_DISPLAYING, handle);
};

/**
 * @private
 */
const state = {
    activeAppId: '',
    patient: null,
};
/**
 * Trigger an event to notify apps of a display change from Novo Bridge
 *
 * @param appId
 *
 * @private
 */
const setActiveApp = (appId) => {
    if (state.activeAppId) {
        emitToApp(EVENTS.DISPLAY_CHANGED, false, state.activeAppId);
    }
    state.activeAppId = appId;
    log(LOG_DEBUG, 'bridge :: setActiveApp');
    console.log('SETTING ACTIVE STATE TO TRUE', state.activeAppId);
    emitToApp(EVENTS.DISPLAY_CHANGED, true, state.activeAppId);
};

/**
 * Invoked when application is shown or hidden in bridge
 *
 * @param handle
 *
 * @private
 */
const onDisplayChanged = (handle) => {
    return on(EVENTS.DISPLAY_CHANGED, handle);
};
/**
 * Subscribe to the patient change event
 *
 * @param handle
 *
 * @private
 */
const onPatientChanged = (handle) => {
    return on(REQUESTS.UPDATE_PATIENT, handle);
};

/**
 * Trigger an event to send authorization token. Applicable to Novo applications only.
 *
 * @param authToken
 *
 * @private
 */
const updateAuth = (authToken) => {
    emitToParent(REQUESTS.UPDATE_AUTH, authToken);
    log(LOG_DEBUG, 'bridge :: updateAuth:', authToken);
};
/**
 * Request from app to get display status
 *
 * @param appId
 *
 * @private
 */
const isDisplaying = () => {
    return new Promise((resolve) => {
        const off = onDisplayChanged((isDisplaying) => {
            off();
            resolve(isDisplaying);
        });
        emitToParent(REQUESTS.IS_DISPLAYING);
    });
};
/**
 * Request from app to get the current patient
 *
 * @returns
 *
 * @private
 */
const getPatient = () => {
    return new Promise((resolve) => {
        const off = onPatientChanged((patient) => {
            off();
            resolve(patient);
        });
        emitToParent(REQUESTS.GET_PATIENT);
    });
};
/**
 * Request to bridge to show an a loaded application
 *
 * @param appId
 *
 * @private
 */
const showApp = (appId) => {
    log(LOG_DEBUG, window.name, ':: showApp:', appId);
    emitToParent(REQUESTS.SHOW_APP, { appId });
};
/**
 * Notify bridge of a count change. Bridge will place an indicator on the icon
 * representing the application
 *
 * @param count
 *
 * @private
 */
const updateBadgeCount = (count = 0) => {
    log(LOG_DEBUG, window.name, ':: updateBadge:', 'count:', count);
    emitToParent(REQUESTS.UPDATE_BADGE_COUNT, count);
};

/**
 * Bridge SDK
 *
 * @copyright 2020-2021. All rights reserved.
 */
// :: App methods ::
/**
 * Request to get current patient
 */
const getPatient$1 = getPatient;
/**
 * Request to get the display status of particular application
 */
const isDisplaying$1 = isDisplaying;
/**
 * Request to display a particular application
 */
const showApp$1 = showApp;
/**
 * @private
 */
const updateAuth$1 = updateAuth;
/**
 * Request to update the badge indicator on icon representing the application
 */
const updateBadgeCount$1 = updateBadgeCount;
// :: App hooks ::
/**
 * Dispatched when the app visibility changes
 *
 * @returns off() - unsubscribe from event
 */
const onDisplayChanged$1 = onDisplayChanged;
/**
 * Dispatched when the patient changes
 *
 * @returns off() - unsubscribe from event
 */
const onPatientChanged$1 = onPatientChanged;
// :: Bridge methods ::
/**
 * Loads an application into a designated element
 *
 * @private
 */
const loadApp$1 = loadApp;
/**
 * @private
 */
const setActiveApp$1 = setActiveApp;
// :: Bridge hooks ::
/**
 * Dispatched when the authentication changes
 *
 * @returns off() - unsubscribe from event
 *
 * @private
 */
const onAuthChanged$1 = onAuthChanged;
/**
 * Dispatched when the badge count changes
 *
 * @returns off() - unsubscribe from event
 *
 * @private
 */
const onBadgeCountChanged$1 = onBadgeCountChanged;
onGetDisplayStatus((request) => {
    emitToApp(EVENTS.DISPLAY_CHANGED, request.app === state.patient, request.app);
});
onGetPatient((request) => {
    emitToApp(EVENTS.PATIENT_CHANGED, state.patient, request.app);
});

exports.getPatient = getPatient$1;
exports.isDisplaying = isDisplaying$1;
exports.loadApp = loadApp$1;
exports.onAuthChanged = onAuthChanged$1;
exports.onBadgeCountChanged = onBadgeCountChanged$1;
exports.onDisplayChanged = onDisplayChanged$1;
exports.onPatientChanged = onPatientChanged$1;
exports.setActiveApp = setActiveApp$1;
exports.showApp = showApp$1;
exports.updateAuth = updateAuth$1;
exports.updateBadgeCount = updateBadgeCount$1;
