# Bridge JavaScript SDK

The Bridge JavaScript SDK is provides applications the ability to integrate into Novo Bridge.

### Requirements

Your application needs to be registered with the Novo Marketplace in order for it to be available for
health care professionals to use in their EMRs. Go to https://marketplace.novo.health to register your application.

### Setup

#### NPM

Install the npm package by using either yarn or npm

```
$ yarn add @novohealth/novo-bridge-js-sdk
```

```
$ npm i @novohealth/novo-bridge-js-sdk
```

You can import the entire module or individual exports

```js
import * as bridgeSDK from '@novohealth/novo-bridge-js-sdk'
```

```js
import { ready, onPatientChanged } from '@novohealth/novo-bridge-js-sdk'
```

#### CDN

To load the file via our CDN, go to the SDK website.

https://bridge-js-sdk.novo.health

### Usage

```js
// Component that loads the apps for NovoBridge
import {
  ready,
  onPatientChanged,
  updateBadgeCount,
} from '@novohealth/novo-bridge-js-sdk'

const initApp = async () => {
  const urlParams = new URLSearchParams(window.location.search)

  // Bridge will append mode=bridge to the url so you can determine your
  // application resides within the bridge environment

  if (urlParams.get('mode') == 'bridge') {
    onPatientChanged((emrPatient) => {
      console.log('EMR PATIENT:', emrPatient.id, emrPatient.name)
    })

    ready(() => {
      updateBadgeCount(1)
    })
  }
}
```

### Testing your application with Novo Bridge

Use the following sandbox to test your application's integration.

[Go to sandbox](https://bridge-js-sdk.novo.health/sandbox)

Novo Health (c) 2021. All rights reserved.
