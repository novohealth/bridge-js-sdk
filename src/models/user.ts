/**
 * Represents the patient that is currently active
 *
 */
export interface User {
  /**
   * Represents the username of the authenticated user
   */
  username: string

  /**
   * Represents the EHR user authenticated into
   */
  ehr: string
}
