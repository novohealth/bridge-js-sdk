/**
 * Represents the patient that is currently active
 *
 */
export interface EmrPatient {
  /**
   * Represents the id of the patient provided by the EMR
   */
  id: string

  /**
   * Represents the name of the patient provided by the EMR
   */
  name: string
}
