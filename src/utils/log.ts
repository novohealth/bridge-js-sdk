import { LOG_NONE } from '../consts'

let _logLevel = LOG_NONE

/**
 * Sets the log level for logging
 * 
 * @param val LOG_DEBUG | LOG_ERROR | LOG_NONE
 */
export function setLogLevel(val: number) {
  _logLevel = val
}

/**
 * Logs items to console window in the browser
 * 
 * @param logLevel log level for logging
 * @param rest all the argument followed will be printed to the console
 */
export function log(logLevel: number, ...rest) {
  if (_logLevel >= logLevel) {
    console.log(...rest)
  }
}
