import { EVENTS } from '../consts'

const origins = {}
const PARENT_ORIGIN_CHANGED = 'parentOriginChanged'
const PARENT = 'parent'
const PREFIX = '$$'

export const isReady = (): boolean => {
  return !!origins[PARENT]
}

export const emitToApps = (event: string, data: any) => {
  let names = Object.keys(origins)
  names.forEach((name) => {
    emitToApp(event, data, name)
  })
}

export const emitToApp = (event: string, data: any, name: string) => {
  const frame = window.frames[name]
  if (frame) {
    frame.postMessage(
      JSON.stringify({
        data,
        event,
      }),
      origins[name]
    )
  }
}

export const emitToParent = (event: string, data = null) => {
  window.parent.window.postMessage(
    JSON.stringify({
      app: window.name,
      data,
      event,
    }),
    origins[PARENT]
  )
}

export const on = (event: string, handle: Function): Function => {
  const f = (payload: any) => {
    if (payload.app) {
      // dispatch to parent
      handle({
        app: payload.app,
        data: payload.data,
      })
    } else {
      // dispatch to child
      handle(payload.data)
    }
  }
  window.addEventListener(PREFIX + event, (evt: CustomEvent) => {
    f(evt.detail || {})
  })

  return () => {
    return window.removeEventListener(PREFIX + event, f)
  }
}

export const loadApp = (name: string, url: string, targetEl: Element) => {
  origins[name] = new URL(url).origin
  return new Promise<void>((resolve, reject) => {
    const frame = document.createElement('iframe')
    frame.name = name
    frame.src = url
    // frame.setAttribute('scrolling', 'no')
    frame.setAttribute('marginheight', '0')
    frame.setAttribute('frameborder', '0')
    frame.setAttribute('allowtransparency', 'true')
    frame.setAttribute(
      'allow',
      'camera *;microphone *;clipboard-read *;clipboard-write *'
    )
    frame.width = '100%'
    frame.height = '100%'
    frame.addEventListener(
      'load',
      () => {
        emitToApp(PARENT_ORIGIN_CHANGED, location.origin, name)
        return resolve()
      },
      false
    )
    targetEl.append(frame)
  })
}

on(PARENT_ORIGIN_CHANGED, (origin: string) => {
  origins[PARENT] = origin
  window.dispatchEvent(new CustomEvent(PREFIX + EVENTS.READY))
})

window.addEventListener(
  'message',
  (evt) => {
    if (evt.origin !== location.origin) {
      let payload
      try {
        if (typeof evt.data === 'string') {
          payload = JSON.parse(evt.data)
          window.dispatchEvent(
            new CustomEvent(PREFIX + payload.event, { detail: payload })
          )
        }
      } catch (e) {
        // console.warn('[ MINGLE PARSING EVENT DATA ] ::', evt.data)
      }
    }
  },
  false
)
