/**
 * Bridge SDK
 *
 * @copyright 2020-2021. All rights reserved.
 */
import { EVENTS } from './consts'
import { setLogLevel as _setLogLevel } from './utils/log'
import { emitToApp, loadApp as _loadApp } from './utils/mingle'
import {
  onAuthChanged as _onAuthChanged,
  onBadgeCountChanged as _onBadgeCountChanged,
  onGetDisplayStatus as _onGetDisplayStatus,
  onGetPatient as _onGetPatient,
  onShowApp as _onShowApp,
  onNotify as _onNotify,
} from './hooks/bridge'
import {
  state,
  setActiveApp as _setActiveApp,
  updatePatient as _updatePatient,
} from './store/bridge'
import {
  getPatient as _getPatient,
  getUser as _getUser,
  isDisplaying as _isDisplaying,
  showApp as _showApp,
  updateAuth as _updateAuth,
  updateBadgeCount as _updateBadgeCount,
  notify as _notify,
} from './store/app'
import {
  onDisplayChanged as _onDisplayChanged,
  onPatientChanged as _onPatientChanged,
} from './hooks/app'

// :: App methods ::

/**
 * Request to get current user
 */
export const getUser = _getUser

/**
 * Request to get current patient
 */
export const getPatient = _getPatient

/**
 * Request to get the display status of particular application
 */
export const isDisplaying = _isDisplaying

/**
 * Request to display a particular application
 */
export const showApp = _showApp

/**
 * @private
 */
export const updateAuth = _updateAuth

/**
 * Request to update the badge indicator on icon representing the application
 */
export const updateBadgeCount = _updateBadgeCount

/**
 * Request to push a notification to be shown as a bridge notification with app icon
 */
export const notify = _notify

// :: App hooks ::

/**
 * Dispatched when the app visibility changes
 *
 * @returns off() - unsubscribe from event
 */
export const onDisplayChanged = _onDisplayChanged

/**
 * Dispatched when the patient changes
 *
 * @returns off() - unsubscribe from event
 */
export const onPatientChanged = _onPatientChanged

// :: Bridge methods ::

/**
 * Loads an application into a designated element
 *
 * @private
 */
export const loadApp = _loadApp

/**
 * @private
 */
export const setActiveApp = _setActiveApp

/**
 * @private
 */
export const updatePatient = _updatePatient

// :: Bridge hooks ::

/**
 * Dispatched when the authentication changes
 *
 * @returns off() - unsubscribe from event
 *
 * @private
 */
export const onAuthChanged = _onAuthChanged

/**
 * Dispatched when the badge count changes
 *
 * @returns off() - unsubscribe from event
 *
 * @private
 */
export const onBadgeCountChanged = _onBadgeCountChanged

/**a notification has been pushed to bridge
 *
 * @returns off() - unsubscribe from event
 *
 * @private
 */
export const onNotify = _onNotify

/**
 * Dispatched when a specific app has been requested
 *
 * @returns off() - unsubscribe from event
 *
 * @private
 */
export const onShowApp = _onShowApp

_onGetDisplayStatus((request: any) => {
  emitToApp(EVENTS.DISPLAY_CHANGED, request.app === state.patient, request.app)
})

_onGetPatient((request: any) => {
  emitToApp(EVENTS.PATIENT_CHANGED, state.patient, request.app)
})
