import { LOG_DEBUG, REQUESTS } from '../consts'
import {
  onDisplayChanged,
  onPatientChanged,
  onReady,
  onUserChanged,
} from '../hooks/app'
import { EmrPatient } from '../models/patient'
import { User } from '../models/user'
import { log } from '../utils/log'
import { emitToParent, isReady } from '../utils/mingle'

/**
 * Trigger an event to send authorization token. Applicable to Novo applications only.
 *
 * @param authToken
 *
 * @private
 */
export const updateAuth = (authToken: string) => {
  onReady(() => {
    emitToParent(REQUESTS.UPDATE_AUTH, authToken)
    log(LOG_DEBUG, 'bridge :: updateAuth:', authToken)
  })
}

/**
 * Request from app to get display status
 *
 * @param appId
 *
 * @private
 */
export const isDisplaying = (): Promise<boolean> => {
  return new Promise((resolve) => {
    onReady(() => {
      const off = onDisplayChanged((isDisplaying: boolean) => {
        off()
        resolve(isDisplaying)
      })
    })
  })
}

export const getUser = (): Promise<User> => {
  return new Promise((resolve) => {
    onReady(() => {
      const off = onUserChanged((user: User) => {
        off()
        resolve(user)
      })
      emitToParent(REQUESTS.GET_USER)
    })
  })
}

/**
 * Request from app to get the current patient
 *
 * @returns
 *
 * @private
 */
export const getPatient = (): Promise<EmrPatient> => {
  return new Promise((resolve) => {
    onReady(() => {
      const off = onPatientChanged((patient: EmrPatient) => {
        off()
        resolve(patient)
      })
      emitToParent(REQUESTS.GET_PATIENT)
    })
  })
}

/**
 * Request to bridge to show an a loaded application
 *
 * @param appId
 *
 * @private
 */
export const showApp = (appId: string, data: any = null) => {
  onReady(() => {
    log(LOG_DEBUG, window.name, ':: showApp:', appId, data)
    emitToParent(REQUESTS.SHOW_APP, { appId, data })
  })
}

/**
 * Notify bridge of a count change. Bridge will place an indicator on the icon
 * representing the application
 *
 * @param count
 *
 * @private
 */
export const updateBadgeCount = (count: number = 0): void => {
  onReady(() => {
    log(LOG_DEBUG, window.name, ':: updateBadge:', 'count:', count)
    emitToParent(REQUESTS.UPDATE_BADGE_COUNT, count)
  })
}

/**
 * Notify bridge of a notification. Bridge will add the notification to the notifications array
 * with icon representing the application
 *
 * @param count
 *
 * @private
 */
export const notify = (text: string = ''): void => {
  onReady(() => {
    log(LOG_DEBUG, window.name, ':: notify:', 'text:', text)
    emitToParent(REQUESTS.NOTIFY, text)
  })
}
