import { EVENTS, LOG_DEBUG } from '../consts'
import { EmrPatient } from '../models/patient'
import { User } from '../models/user'
import { log } from '../utils/log'
import { emitToApp, emitToApps } from '../utils/mingle'

/**
 * @private
 */
export const state = {
  activeAppId: '',
  patient: null,
  user: null,
}

/**
 * @private
 */
export const setUser = (user: User) => {
  state.user = user
  emitToApps(EVENTS.USER_CHANGED, user)
}

/**
 * @private
 */
export const setActiveAppId = (appId: string) => {
  state.activeAppId = appId
}

/**
 * @private
 */
export const setPatient = (patient: EmrPatient) => {
  state.patient = patient
}

/**
 * Trigger an event to notify apps of a display change from Novo Bridge
 *
 * @param appId
 *
 * @private
 */
export const setActiveApp = (appId: string) => {
  if (state.activeAppId) {
    emitToApp(EVENTS.DISPLAY_CHANGED, false, state.activeAppId)
  }
  state.activeAppId = appId
  log(LOG_DEBUG, 'bridge :: setActiveApp')
  emitToApp(EVENTS.DISPLAY_CHANGED, true, state.activeAppId)
}

/**
 * Trigger an event to update patient in Novo Bridge.
 *
 * @param patient
 *
 * @private
 */
export const updatePatient = (patient: EmrPatient) => {
  state.patient = patient
  log(LOG_DEBUG, 'bridge :: updatePatient:', patient)
  emitToApps(EVENTS.PATIENT_CHANGED, patient)
}
