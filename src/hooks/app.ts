import { EVENTS } from '../consts'
import { EmrPatient } from '../models/patient'
import { User } from '../models/user'
import { isReady, on } from '../utils/mingle'

/**
 * Invoked when application is connected to bridge
 *
 * @param handle
 *
 * @private
 */
export const onReady = (handle: Function) => {
  if (isReady()) {
    handle()
  } else {
    const off = on(EVENTS.READY, () => {
      off()
      handle()
    })
  }
}

/**
 * Invoked when application is shown or hidden in bridge
 *
 * @param handle
 *
 * @private
 */
export const onDisplayChanged = (
  handle: (isDisplaying: boolean) => void
): Function => {
  return on(EVENTS.DISPLAY_CHANGED, handle)
}

/**
 * Subscribe to the patient change event
 *
 * @param handle
 *
 * @private
 */
export const onPatientChanged = (
  handle: (patient: EmrPatient) => void
): Function => {
  return on(EVENTS.PATIENT_CHANGED, handle)
}

/**
 * Subscribe to the user change event
 *
 * @param handle
 *
 * @private
 */
export const onUserChanged = (handle: (user: User) => void): Function => {
  return on(EVENTS.USER_CHANGED, handle)
}
