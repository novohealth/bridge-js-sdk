import { LOG_DEBUG, REQUESTS } from '../consts'
import { log } from '../utils/log'
import { on } from '../utils/mingle'

/**
 * Subscribe to the auth user change event. Applicable to Novo applications only.
 *
 * @param handle
 *
 * @private
 */
export const onAuthChanged = (
  handle: (authToken: string) => void
): Function => {
  log(LOG_DEBUG, 'bridge :: onAuthChanged:', handle)
  return on(REQUESTS.UPDATE_AUTH, handle)
}

/**
 * Bridge subscribes in case there is a request to show an app
 *
 * @private
 */
export const onShowApp = (handle: Function): Function => {
  return on(REQUESTS.SHOW_APP, handle)
}

/**
 * Subscribe when application badge count changes
 *
 * @param handle
 *
 * @private
 */
export const onBadgeCountChanged = (handle: Function): Function => {
  return on(REQUESTS.UPDATE_BADGE_COUNT, handle)
}

/**
 * Subscribe when application requests current patient
 *
 * @param handle
 * @returns
 *
 * @private
 */
export const onGetPatient = (handle: Function): Function => {
  return on(REQUESTS.GET_PATIENT, handle)
}

/**
 * Subscribe when application requests current display status
 *
 * @param handle
 * @returns
 *
 * @private
 */
export const onGetDisplayStatus = (handle: Function): Function => {
  return on(REQUESTS.IS_DISPLAYING, handle)
}

/**
 * Subscribe when applications push notifications
 *
 * @param handle
 *
 * @private
 */
export const onNotify = (handle: Function): Function => {
  return on(REQUESTS.NOTIFY, handle)
}
