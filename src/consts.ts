/**
 * @internal
 */
export const LOG_DEBUG = 2
export const LOG_ERROR = 1
export const LOG_NONE = 0

export const REQUESTS = {
  GET_PATIENT: 'get_patient',
  GET_USER: 'get_user',
  IS_DISPLAYING: 'is_displaying',
  NOTIFY: 'notify',
  SHOW_APP: 'show_app',
  UPDATE_AUTH: 'update_auth',
  UPDATE_PATIENT: 'update_patient',
  UPDATE_BADGE_COUNT: 'update_badge_count',
}

export const EVENTS = {
  AUTH_CHANGED: 'auth_changed',
  BADGE_COUNT_CHANGED: 'badge_count_changed',
  DISPLAY_CHANGED: 'display_changed',
  PATIENT_CHANGED: 'patient_changed',
  READY: 'ready',
  USER_CHANGED: 'user_changed',
}
