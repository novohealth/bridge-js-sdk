# identity-js changelog

## 1.2.0

*2021-01-21*

* Added auth()
* Added setLogLevel()

## 1.1.0

*2021-01-18*

* Added removeCookie

## 1.0.1

*2021-01-14*

* Added firstName and lastName getters to Claims

## 1.0.0

*2021-01-07*

* Initial release