import replace from 'rollup-plugin-replace'
import typescript from '@rollup/plugin-typescript'
import { terser } from 'rollup-plugin-terser'
import clear from 'rollup-plugin-clear'
import copy from 'rollup-plugin-copy'
// import html from '@rollup/plugin-html'
import filesize from 'rollup-plugin-filesize'
import dotenvLoad from 'dotenv-load'
const pkg = require('./package.json')

dotenvLoad()

export default {
  input: 'src/bridge.ts',
  output: [
    {
      file: `dist/${pkg.version}/bridge.js`,
      format: 'iife',
      name: 'bridge',
    },
    {
      file: `dist/${pkg.version}/bridge.min.js`,
      format: 'iife',
      name: 'bridge',
      plugins: [terser()],
      sourcemap: true, // "inline"
    },
    {
      file: `dist/${pkg.version}/bridge.common.js`,
      format: 'cjs',
      name: 'index',
    },
    {
      // this is for publishing to npm
      file: `dist/pkg/index.js`,
      format: 'cjs',
      name: 'index',
    },
  ],
  plugins: [
    clear({
      // required, point out which directories should be clear.
      targets: ['dist/js'],
      // optional, whether clear the directores when rollup recompile on --watch mode.
      watch: true, // default: false
    }),
    replace({
      include: 'src/**',
      delimiters: ['ENV_', ''],
      values: process.env,
    }),
    typescript({ lib: ['dom'], target: 'es2017' }),
    copy({
      targets: [
        {
          src: `src/sandbox`,
          dest: `dist/${pkg.version}`,
        },
      ],
    }),
    // html(),
    filesize(),
  ],
}
